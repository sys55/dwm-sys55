void 
togglefullscreen(const Arg *arg){
		if (selmon->sel !=  NULL)
				setfullscreen(selmon->sel, !selmon->sel->isfullscreen);
}

void 
toggletagpreview(const Arg *arg){
	selmon->previewshow = !selmon->previewshow;
			
	showtagpreview(arg->i);
}

void 
viewtag(const Arg *arg){
		toggletagpreview(arg); // for not including the preview win in the preview 
		view(arg);
}


/* PATCHES */

/* horizgrid */
void
horizgrid(Monitor *m) {
	Client *c;
	unsigned int n, i;
	int w = 0;
	int ntop, nbottom = 0;
	int mw, my, ty;

	/* Count windows */
	for(n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++);

	if (n > m->nmaster)
		mw = m->nmaster ? m->ww * m->mfact : 0;
	else
		mw = m->ww - m->gappx;

	my = ty = m->gappx;

	if(n == 0)
		return;
	else if(n == 1) { /* Just fill the whole screen */
		c = nexttiled(m->clients);
		resize(c, m->wx, m->wy, m->ww - (2*c->bw), m->wh - (2*c->bw), False);
	} else if(n == 2) { /* Split vertically */
		w = m->ww / 2;
		c = nexttiled(m->clients);
		resize(c, m->wx, m->wy, w - (2*c->bw), m->wh - (2*c->bw), False);
		c = nexttiled(c->next);
		resize(c, m->wx + w, m->wy, w - (2*c->bw), m->wh - (2*c->bw), False);
	} else {
		ntop = n / 2;
		nbottom = n - ntop;
		for(i = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), i++) {
			if(i < ntop)
				resize(c, m->wx + i * m->ww / ntop, m->wy, m->ww / ntop - (2*c->bw), m->wh / 2 - (2*c->bw), False);
			else
				resize(c, m->wx + (i - ntop) * m->ww / nbottom, m->wy + m->wh / 2, m->ww / nbottom - (2*c->bw), m->wh / 2 - (2*c->bw), False);
		}
	}
}
/* horizgrid */


/* colors */
#ifdef COLORS


#define ALICE "#F0F8FF"
#define ANTIQUE "#FAEBD7"
#define AQUA "#7FFFD4"
#define AZURE "#F0FFFF"
#define BEIGE "#F5F5DC"
#define BISQUE "#FFE4C4"
#define BLACK "#000000"
#define BLANCHED "#FFEBCD"
#define BLUE "#0000FF"
#define BROWN "#A52A2A"
#define BURLY "#DEB887"
#define CADET  "#5F9EA0"
#define CHART  "#7FFF00"
#define CHOCOLATE "#D2691E"
#define CORAL "#FF7F50"
#define CORN "#FFF8DC"
#define CRIMSON "#DC143C"
#define CYAN "#00FFFF"
#define DARK "#FF8C00"
#define DEEP "#00BFFF"
#define DIM "#696969"
#define DODGER "#1E90FF"
#define FIREBRICK "#B22222"
#define FLORAL "#FFFAF0"
#define FOREST "#228B22"
#define GAINSBORO "#DCDCDC"
#define GHOST "#F8F8FF"
#define GOLD "#FFD700"
#define GOLDEN "#DAA520"
#define GRAY "#808080"
#define GREEN "#ADFF2F"
#define HONEYDEW "#F0FFF0"
#define HOT  "#FF69B4"
#define INDIAN "#CD5C5C"
#define INDIGO "#4B0082"
#define IVORY "#FFFFF0"
#define KHAKI "#F0E68C"
#define LAVENDER "#FFF0F5"
#define LAWN  "#7CFC00"
#define LEMON "#FFFACD"
#define LIGHT "#FFFFE0"
#define LIME "#00FF00"
#define LINEN "#FAF0E6"
#define MAGENTA "#FF00FF"
#define MAROON "#800000"
#define MEDIUM "#C71585"
#define MIDNIGHT "#191970"
#define MINT "#F5FFFA"
#define MISTY "#FFE4E1"
#define MOCCASIN "#FFE4B5"
#define NAVAJO "#FFDEAD"
#define NAVY "#000080"
#define OLD "#FDF5E6"
#define OLIVE "#6B8E23"
#define ORANGE "#FF4500"
#define ORCHID "#DA70D6"
#define PALE  "#98FB98"
#define PAPAYA "#FFEFD5"
#define PEACH  "#FFDAB9"
#define PERU "#CD853F"
#define PINK "#FFC0CB"
#define PLUM "#DDA0DD"
#define POWDER "#B0E0E6"
#define PURPLE "#800080"
#define RED "#FF0000"
#define ROSY  "#BC8F8F"
#define ROYAL  "#4169E1"
#define SADDLE "#8B4513"
#define SALMON "#FA8072"
#define SANDY "#F4A460"
#define SEA "#FFF5EE"
#define SILVER "#C0C0C0"
#define SKY  "#87CEEB"
#define SLATE "#708090"
#define SNOW "#FFFAFA"
#define SPRING "#00FF7F"
#define STEEL "#4682B4"
#define TAN "#D2B48C"
#define TEAL "#008080"
#define THISTLE "#D8BFD8"
#define TOMATO "#FF6347"
#define TURQUOISE "#40E0D0"
#define VIOLET "#EE82EE"
#define WHEAT "#F5DEB3"
#define WHITE "#FFFFFF"
#define YELLOW "#9ACD32"

#endif
